# ======================================================================================================================
# Runners
# ======================================================================================================================
.ruby_runner:
  image: registry.gitlab.com/dependabot-gitlab/dependabot/ruby:2.6.7-buster-8ad50495
  variables:
    BUNDLE_PATH: vendor/bundle
    BUNDLE_FROZEN: "true"
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - bundle install
  cache:
    key:
      files:
        - Gemfile.lock
    paths:
      - vendor/bundle
      - codacy-coverage-reporter-$CODACY_VERSION
    policy: pull

.deploy_runner:
  image: quay.io/roboll/helmfile:helm3-v0.138.7

.buildkit_runner:
  image:
    name: moby/buildkit:v0.8.2
    entrypoint: [""]
  before_script:
    - mkdir -p /root/.docker
    - |
      cat <<- EOF > /root/.docker/config.json
      {
        "auths": {
          "registry.gitlab.com": {
            "auth": "$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)"
          }
        }
      }
      EOF
  script:
    - script/build.sh

# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - static analysis
  - test
  - report
  - release
  - deploy

variables:
  GITLAB_MOCK_DOCKER_IMAGE: $CI_REGISTRY_IMAGE/gitlab-mock
  CURRENT_TAG: $CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
  LATEST_TAG: $CI_COMMIT_REF_SLUG-latest
  CODACY_VERSION: "11.2.3"
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CONTAINER_SCANNING_DISABLED: "true"

# ======================================================================================================================
# Jobs
# ======================================================================================================================
.cache_dependencies:
  stage: .pre
  extends: .ruby_runner
  script:
    - ./script/download-codacy.sh
  cache:
    policy: pull-push

.build_app_image:
  stage: .pre
  extends: .buildkit_runner
  variables:
    DOCKER_CONTEXT: "."
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE
    PUSH: "true"

.build_mock_image:
  stage: .pre
  extends: .buildkit_runner
  variables:
    DOCKER_CONTEXT: spec/fixture/gitlab
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/gitlab-mock
    PUSH: "true"

.build_ruby_image:
  stage: .pre
  extends: .buildkit_runner
  variables:
    DOCKER_CONTEXT: docker
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/ruby
    PUSH: "false"

.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.bundle_audit:
  stage: static analysis
  extends: bundler-audit-dependency_scanning

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning
  variables:
    DS_REMEDIATE: "false"

.container_scanning:
  stage: static analysis
  extends: container_scanning
  variables:
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA

.rspec:
  stage: test
  extends: .ruby_runner
  services:
    - name: bitnami/redis:6.0-debian-10
      alias: redis
    - name: bitnami/mongodb:4.4-debian-10
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    COVERAGE: "true"
    MAX_ROWS: 5
    OUTPUT_STYLE: block
  script:
    - bundle exec rspec --format documentation --format RspecJunitFormatter --out tmp/rspec.xml
  after_script:
    - ./codacy-coverage-reporter-$CODACY_VERSION report -r coverage/coverage.xml
  artifacts:
    reports:
      cobertura: coverage/coverage.xml
      junit: tmp/rspec.xml
    paths:
      - coverage/.resultset.json
      - reports/allure-results
    expire_in: 7 days

.coverage:
  stage: report
  extends: .ruby_runner
  variables:
    NO_COLOR: 1
    MAX_ROWS: 1
    OUTPUT_STYLE: block
  script:
    - bundle exec rake coverage
.coverage_line:
  extends: .coverage
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
.coverage_branch:
  extends: .coverage
  coverage: /^BRANCH COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/

.allure_report:
  stage: report
  image:
    name: andrcuns/allure-report-publisher:0.0.4
    entrypoint: [""]
  variables:
    GITLAB_AUTH_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - |
      allure-report-publisher upload s3 \
        --results-glob="reports/allure-results/*" \
        --bucket="allure-reports-andrcuns" \
        --prefix="dependabot-gitlab/$CI_COMMIT_REF_SLUG" \
        --update-pr \
        --copy-latest \
        --color

.e2e:
  stage: test
  image:
    name: $CI_REGISTRY_IMAGE:$CURRENT_TAG
    entrypoint: [""]
  services:
    - name: $GITLAB_MOCK_DOCKER_IMAGE:$CURRENT_TAG
      alias: gitlab
  variables:
    SETTINGS__GITLAB_URL: http://gitlab:4567
    SETTINGS__GITLAB_ACCESS_TOKEN: e2e-test
    SETTINGS__STANDALONE: "true"
    SETTINGS__LOG_LEVEL: "debug"
    RAILS_ENV: test
    GIT_STRATEGY: none
  script:
    - cd /home/dependabot/app && bundle exec rake 'dependabot:update[test-repo,bundler,/]'

.release_image:
  stage: release
  image: docker:20.10.5
  services:
    - docker:20.10.5-dind
  variables:
    RELEASE_IMAGE: docker.io/andrcuns/dependabot-gitlab
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
    DOCKER_CERT_PATH: /certs/client
    DOCKER_TLS_VERIFY: 1
    DOCKER_BUILDKIT: 1
  before_script:
    - while ! test -f "$DOCKER_CERT_PATH/ca.pem"; do sleep 2; done
    - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_PASSWORD
  script:
    - |
      apk -qq update && apk -qq add grep
      export RELEASE_VERSION=$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+')
    - |
      docker pull $CI_REGISTRY_IMAGE:$CURRENT_TAG
      docker tag $CI_REGISTRY_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:$RELEASE_VERSION
      docker tag $CI_REGISTRY_IMAGE:$CURRENT_TAG $RELEASE_IMAGE:latest
      docker push $RELEASE_IMAGE:$RELEASE_VERSION && docker push $RELEASE_IMAGE:latest

.gitlab_release:
  stage: release
  extends: .ruby_runner
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:gitlab[$CI_COMMIT_TAG]"

.update_chart:
  extends: .ruby_runner
  stage: release
  before_script:
    - source script/setup-git.sh
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"

.update_standalone:
  extends: .ruby_runner
  stage: release
  variables:
    SETTINGS__GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"

.deploy:
  extends: .deploy_runner
  stage: deploy
  script:
    - cd kube/helm
    - helmfile repos
    - helmfile apply --context 10

.stop:
  extends: .deploy_runner
  stage: deploy
  environment:
    action: stop
  variables:
    GIT_STRATEGY: none
  script:
    - helm uninstall dependabot-gitlab -n dependabot

.release_ruby_image:
  stage: release
  extends: .build_ruby_image
  variables:
    CURRENT_TAG: 2.6.7-buster-$CI_COMMIT_SHORT_SHA
    LATEST_TAG: 2.6.7-buster-latest
    PUSH: "true"
